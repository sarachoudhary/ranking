file = 'marks_scored.txt'

class Student:
    def __init__(self, name, scores):
        self.name = name
        self.scores = scores

def parse_file(file):
    students = []
    with open(file, 'r') as file:
        for line in file:
            parts = line.strip().split(' ')
            name = parts[0]
            scores = list(map(int, parts[1:]))
            students.append(Student(name, scores))
    return students

def compare_students(student1, student2):
    all_greater = all(score1 > score2 for score1, score2 in zip(student1.scores, student2.scores))
    if all_greater:
        return -1
    all_lesser = all(score1 < score2 for score1, score2 in zip(student1.scores, student2.scores))
    if all_lesser:
        return 1
    return 0
